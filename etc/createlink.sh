#!/bin/bash
ln -sf $(pwd)'/dhcpd.conf' /etc/dhcp/dhcpd.conf
ln -sf $(pwd)'/exports' /etc/exports
ln -sf $(pwd)'/site-nginx' /etc/nginx/sites-available/site-nginx
ln -sf $(pwd)'/tftpd-hpa' /etc/default/tftpd-hpa
