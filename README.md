Ipxe szerver útmotató.
----------------------

### Szügséges telepittet csomagok.
	
- nginix-light
- isc-dhcp 
- tftpd-hpa
- nfs-kernel-server
	
### Felhasznált forráskódok.

 - http://ipxe.org/start 
 - https://github.com/antonym/netboot.xyz

	
Az ipxe.org on rengeteg példa található a használatra. Sajnos nem minden naprakész.
	
Ezen forrás használata a '/opt' könyvtárból való futásra van megírva.
Amenyiben máshonan futtatnánk az öszes linket át kell irni a konfigfájlokban.

### Alábbiakban a konfigfájlok működési leírása.

*	~etc/ 
	Itt találhatók a szolgáltatások konfigurációi. 
	A 'createlink.sh' autómatikusan létrehoza a konfigurációk linkjeit a szükséges könyvtárstruktúrába.
	
*	~tftp/
	'undionly.kpxe' Ipxe Bináris file. Szükséges forráskótbol fordítani. Csak a legacy bottfojamatért felel. A fellelhető lefordított változatok nem jók.
	'wimboot' wim file kicsomagolásához szükséges bináris.
	
*	~www/
	Az itt található öszes .ipxe a menürendszer.
	'memdisk' iso file bootolo eszköz.
	
*	~www/iso/
	Kicsomagolt, preparált bootcd állományok.
	
Majdnem minden rendszer különböző bootolási mehanizmust használ.
Ezeket nem lehet egységesíteni. Kiindulási pontként nézd meg az ipxe menürendszer fájljait. 

### Sok sikert:
### Szala
